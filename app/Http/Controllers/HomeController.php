<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Menu;
use App\Models\Permission;
use Auth;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('admin.viewpermission');
    }
    
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function adminHome()
    {
        return view('admin.adminHome');
    }

    public function sublogin()
    {
        return view('admin.sublogin');
    }
    public function viewmenu()
    {
        $per = Menu::get();
        return view('admin.viewmenu')->with('per', $per);
    }

    public function addmenu()
    {
        return view('admin.addmenu');
    }

    public function storemenu(Request $request)
    {
        $this->validate($request, ['menu' => ['required']]);
        $input = $request->all();

        $add_feature=[
      
        'menu' => $input['menu'],
              
      
        ];
        $save = Menu::create($add_feature);

        if($save){
        
        return redirect('viewmenu')->with('success','Permission Add Successfull!');
        }
    }
    public function deletmenu(Request $request)
    {
        $id=$request->id;
  
        $rrr=Menu::where('id',$id)->delete();
         if($rrr)
         {
            return back()->with('success','Permission Delete Successfull!');
         }
    }



    public function viewpermission()
    {
        $per = DB::table('permissions')->join('users','permissions.subadmin_id','=','users.id')->get();
        return view('admin.view')->with('per', $per);
    }

    public function addpermission()
    {
        $sub = Auth::user()->where('is_admin',0)->get();
        
        $menu = Menu::get();
        return view('admin.add')->with('sub', $sub)->with('menu', $menu);
    }

    public function storepermission(Request $request)
    {
        
        $input = $request->all();
    //   dd($request->subadmin_id,);
        foreach($input['menu'] as $key=> $menu)
{
        $add_feature=[
      
        'menu_name' =>  $menu,
        'subadmin_id' =>$request->subadmin_id,
              
        ];
        $save = Permission::create($add_feature);
    }
        if($save){
        
        return redirect('viewpermission')->with('success','Permission Add Successfull!');
        }
    }
    public function deletepermission(Request $request)
    {
        $id=$request->id;
  
        $rrr=Menu::where('id',$id)->delete();
         if($rrr)
         {
            return back()->with('success','Permission Delete Successfull!');
         }
    }





}
