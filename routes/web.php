<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

// Auth::routes();

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/home',[App\Http\Controllers\HomeController::class, 'adminHome'])->name('home')->middleware('is_admin');
Route::get('/sublogin',[App\Http\Controllers\HomeController::class, 'sublogin'])->name('sublogin')->middleware('is_admin');
Route::get('/viewmenu',[App\Http\Controllers\HomeController::class, 'viewmenu'])->name('viewmenu')->middleware('is_admin');
Route::get('/addmenu',[App\Http\Controllers\HomeController::class, 'addmenu'])->name('addmenu')->middleware('is_admin');
Route::post('/storemenu',[App\Http\Controllers\HomeController::class, 'storemenu'])->name('storemenu')->middleware('is_admin');
Route::get('/deletmenu',[App\Http\Controllers\HomeController::class, 'deletmenu'])->name('deletmenu')->middleware('is_admin');

Route::get('/viewpermission',[App\Http\Controllers\HomeController::class, 'viewpermission'])->name('viewpermission')->middleware('is_admin');
Route::get('/addpermission',[App\Http\Controllers\HomeController::class, 'addpermission'])->name('addpermission')->middleware('is_admin');
Route::post('/storepermission',[App\Http\Controllers\HomeController::class, 'storepermission'])->name('storepermission')->middleware('is_admin');
Route::get('/deletepermission',[App\Http\Controllers\HomeController::class, 'deletepermission'])->name('deletepermission')->middleware('is_admin');

Route::get('/subadmin', [App\Http\Controllers\HomeController::class, 'index'])->name('subadmin')->middleware('is_subadmin');
