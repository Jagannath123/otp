@extends('layout.menu') @section('title') Admin||View Menu @endsection @section('content')
<div class="page-header mt-0 p-3">
    <h3 class="mb-sm-0">View Menu</h3>
    <ol class="breadcrumb mb-0">
        <li class="breadcrumb-item">
            <a href="/"><i class="fe fe-home"></i></a>
        </li>
        <li class="breadcrumb-item active" aria-current="page">View Menu</li>
    </ol>
</div>
<div class="row">
    <div class="col-md-12">
   
        <div class="card shadow">
            <div class="card-header">
                <a class="mb-0" href="/addmenu">Add Menu</a>
            </div>
            <div class="card-body">
                @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <a type="button" class="close" data-dismiss="alert">×</a>	
                        <strong>{{ $message }}</strong>
                </div>
                @endif
                <div class="table-responsive">
                    <table id="edd" class="table table-striped table-bordered w-100 text-nowrap">
                        <thead>


                       
                            <tr>
                                <th class="wd-5p">Permission</th>
                                
                                <th class="wd-10p">Action</th>
                            </tr>


                        </thead>
                        <tbody>
                        @foreach($per as $perm)
                            <tr>
                                <td>{{$perm->menu}}</td>
                               
                                <td><a  href="/deletpermission?id={{$perm->id}}" class="btn btn-youtube mt-1 mb-1 " ><i class="fa fa-trash"></i>Delete</a>&nbsp;</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $(function () {
            $("#edd").dataTable({
                bPaginate: true,
                bLengthChange: true,
                bFilter: true,
                bSort: false,
                bInfo: true,
                bAutoWidth: false,
            });
        });
    });
</script>
