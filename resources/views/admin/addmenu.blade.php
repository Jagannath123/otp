@extends('layout.menus') @section('title') Admin||Add Menu @endsection @section('content')
<div class="page-header mt-0 p-3">
    <h3 class="mb-sm-0">Add Menu</h3>
    <ol class="breadcrumb mb-0">
        <li class="breadcrumb-item">
            <a href="/"><i class="fe fe-home"></i></a>
        </li>
        <li class="breadcrumb-item active" aria-current="page">Add Menu</li>
    </ol>
</div>
<div class="row">
    <div class="col-md-12">

    <form id="add_menu_code" action="{{Route('storemenu')}}" method="post" enctype="multipart/form-data">
    @csrf
        <div class="card shadow">
            <div class="card-header">
                <h2 class="mb-0">Add Menu</h2>
            </div>
            <div class="card-body">
            @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block">
                <a type="button" class="close" data-dismiss="alert">×</a>	
                    <strong>{{ $message }}</strong>
            </div>
            @endif

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                        <lebel>Enter Menu Name</lebel>
                            <input type="text" class="form-control" id="menu" name="menu" placeholder="Enter Menu Name" value="" required>
                            <div class="text-danger"><strong id="menu_error"></strong></div>
                        </div>
                        
                     
                    </div>
                    
                    
                    <br><br>

                    <div class="col-md-5">
                    </div>
                    <div class="col-md-4" style="    margin-top: 20px !important;">
                    <input type="submit" class ="btn btn-primary mt-1 mb-1" value="Submit" >
                    </div>
                    <div class="col-md-3">
                    </div>
           </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
