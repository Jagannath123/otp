@extends('layout.menus') @section('title') Admin||Add Permission @endsection @section('content')
<div class="page-header mt-0 p-3">
    <h3 class="mb-sm-0">Add Permission</h3>
    <ol class="breadcrumb mb-0">
        <li class="breadcrumb-item">
            <a href="/"><i class="fe fe-home"></i></a>
        </li>
        <li class="breadcrumb-item active" aria-current="page">Add Permission</li>
    </ol>
</div>
<div class="row">
    <div class="col-md-12">

    <form id="add_permission_code" action="{{Route('storepermission')}}" method="post" enctype="multipart/form-data">
    @csrf
        <div class="card shadow">
            <div class="card-header">
                <h2 class="mb-0">Add Permission</h2>
            </div>
            <div class="card-body">
            @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block">
                <a type="button" class="close" data-dismiss="alert">×</a>	
                    <strong>{{ $message }}</strong>
            </div>
            @endif

                <div class="row">
                <div class="col-md-6">
                        <div class="form-group">
                            <lebel>Chose Sub Admin Name</lebel>
                            <select name="subadmin_id" id="subadmin_id" class="form-control custom-select">
                            <option value=" " disable selected ></option>
                                 @foreach($sub as $admin)
                                <option value="{{$admin->id}}">{{$admin->name}}</option>
                                @endforeach
                                
                            </select>
                            
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            <lebel>Chose Menu Name</lebel>
                            <select name="menu[]" id="menu" class="form-control"  multiple>
                            <!-- <option value=" " disable selected ></option> -->
                                @foreach($menu as $menus)
                                <option value="{{$menus->menu}}" >{{$menus->menu}}</option>
                                @endforeach
                                
                            </select>
                           
                        </div>
                    </div>

                    <br><br>

                    <div class="col-md-5">
                    </div>
                    <div class="col-md-4" style="    margin-top: 20px !important;">
                    <input type="submit" class ="btn btn-primary mt-1 mb-1" value="Submit" >
                    </div>
                    <div class="col-md-3">
                    </div>
           </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
