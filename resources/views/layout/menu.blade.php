<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8" />
        <meta content="width=device-width, initial-scale=1, shrink-to-fit=no" name="viewport" />
        <meta content="Fully Responsive Bootstrap 4 Admin Dashboard Template" name="description" />
        <meta content="Spruko" name="author" />

        <!-- Title -->

        <title>@yield('title')</title>

      

        <!-- Favicon -->
        <link href="assets1/img/brand/favicon.png" rel="icon" type="image/png" />

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:300,400,600,700,800" rel="stylesheet" />

        <!-- Icons -->
        <link href="admin/assets1/css/icons.css" rel="stylesheet" />

        <!--Bootstrap.min css-->
        <link rel="stylesheet" href="assets1/plugins/bootstrap/css/bootstrap.min.css" />

        <!-- Dashboard CSS -->
        <link href="assets1/css/dashboard.css" rel="stylesheet" type="text/css" />

        <!-- Tabs CSS -->
        <link href="assets1/plugins/tabs/style.css" rel="stylesheet" type="text/css" />

        <!-- Custom scroll bar css-->
        <link href="assets1/plugins/customscroll/jquery.mCustomScrollbar.css" rel="stylesheet" />

        <!-- Sidemenu Css -->
        <link href="assets1/plugins/toggle-sidebar/css/sidemenu.css" rel="stylesheet" />

        <!-- Data table css -->
        <link href="assets1/plugins/datatable/dataTables.bootstrap4.min.css" rel="stylesheet" />
        <link href="assets1/plugins/datatable/responsivebootstrap4.min.css" rel="stylesheet" />
        <link href="assets1/plugins/customscroll/jquery.mCustomScrollbar.css" rel="stylesheet" />
        {{--
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.1/css/bootstrap.css" />
        --}} @toastr_css
    </head>
    <body class="app sidebar-mini rtl">
        <div id="global-loader"></div>
        <div class="page">
            <div class="page-main">
                <!-- Sidebar menu-->
                <div class="app-sidebar__overlay" data-toggle="sidebar"></div>
                <aside class="app-sidebar">
                    <div class="sidebar-img">
                        <a class="navbar-brand" href="/">
                        <img alt="..." class="navbar-brand-img main-logo" src="assets/img/logo/logo.png"  style=    "width:40%;
                                    height: 100px;
                                    margin-left: 45px;
                                    margin-bottom: -13px;
                                    margin-top: -13px;">
                        </a>
                        <ul class="side-menu">
                            {{--
                            <li class="slide">
                                <a class="side-menu__item" data-toggle="slide" href="#"><i class="side-menu__icon fe fe-home"></i><span class="side-menu__label">Dashboard</span><i class="angle fa fa-angle-right"></i></a>
                                <ul class="slide-menu">
                                    <li>
                                        <a class="slide-item" href="{{route('home')}}">Dashboard</a>
                                    </li>
                                </ul>
                            </li>
                            --}} <li class="slide
                            <?php if(basename($_SERVER['REQUEST_URI'])=="dashboard" || basename($_SERVER['REQUEST_URI'])=="/"){ echo "is-expanded"; }?>">
                            <a class="side-menu__item" href="{{route('home')}}"><i class="side-menu__icon fe fe-home"></i><span class="side-menu__label">Dashboard</span></a>

                            <li class="slide">
                                <a class="side-menu__item"  href="/viewmenu"><i class="side-menu__icon fe fe-file-text"></i><span class="side-menu__label">Menu</span><i class="angle fa fa-angle-right"></i></a>
                                
                            </li>

                            <li class="slide">
                                <a class="side-menu__item"  href="viewpermission"><i class="side-menu__icon fe fe-edit"></i><span class="side-menu__label">Permission</span><i class="angle fa fa-angle-right"></i></a>
                                
                            </li>



                            <li class="slide">
                                <a class="side-menu__item" data-toggle="slide" href="#"><i class="side-menu__icon fe fe-file-text"></i><span class="side-menu__label">User Messages</span><i class="angle fa fa-angle-right"></i></a>
                                <ul class="slide-menu">
                                    <li>
                                        <a href="" class="slide-item">All User Messages</a>
                                    </li>
                                    
                                </ul>
                            </li>

                           
                        </ul>
                    </div>
                </aside>
                <!-- Sidebar menu-->

                <!-- app-content-->
                <div class="app-content">
                    <div class="side-app">
                        <div class="main-content">
                            <div class="p-2 d-block d-sm-none navbar-sm-search">
                                <!-- Form -->
                                <form class="navbar-search navbar-search-dark form-inline ml-lg-auto">
                                    <div class="form-group mb-0">
                                        <div class="input-group input-group-alternative">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fas fa-search"></i></span>
                                            </div>
                                            <input class="form-control" placeholder="Search" type="text" />
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!-- Top navbar -->
                            <nav class="navbar navbar-top navbar-expand-md navbar-dark" id="navbar-main">
                                <div class="container-fluid">
                                    <a aria-label="Hide Sidebar" class="app-sidebar__toggle" data-toggle="sidebar" href="#"></a>
                                    <!-- Horizontal Navbar -->
                                    {{--
                                    <ul class="navbar-nav align-items-center d-none d-xl-block">
                                        <li class="nav-item dropdown">
                                            <a aria-expanded="false" aria-haspopup="true" class="nav-link pr-md-0 mr-md-2 pl-1 d-lg-block" data-toggle="dropdown" href="#" role="button">
                                                Help
                                            </a>
                                        </li>
                                        <li class="nav-item dropdown">
                                            <a aria-expanded="false" aria-haspopup="true" class="nav-link pr-md-0 mr-md-2 pl-1 d-lg-block" data-toggle="dropdown" href="#" role="button">
                                                Mail <span class="badge badge-yellow badge-circle badge-sm h-5 w-5 text-xs">4</span>
                                            </a>
                                            <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
                                                <a href="#" class="dropdown-item d-flex text-center">
                                                    Mail Box
                                                </a>
                                                <a href="#" class="dropdown-item d-flex text-center">
                                                    Compose mail
                                                </a>
                                                <a href="#" class="dropdown-item d-flex text-center">
                                                    Seperated Link
                                                </a>
                                                <div class="dropdown-divider"></div>
                                                <a href="#" class="dropdown-item text-center">View all Notification</a>
                                            </div>
                                        </li>
                                    </ul>
                                    --}}

                                    <!-- Brand -->
                                    <a class="navbar-brand pt-0 d-md-none" href="index.html">
                                        <img src="assets/img/brand/logo-dark1.png" class="navbar-brand-img" alt="..." />
                                    </a>

                                    <!-- Form -->
                                    {{--
                                    <form class="navbar-search navbar-search-dark form-inline ml-3 mr-lg-auto">
                                        <div class="form-group mb-0">
                                            <div class="input-group input-group-alternative">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"><i class="fas fa-search"></i></span>
                                                </div>
                                                <input class="form-control" placeholder="Search" type="text" />
                                            </div>
                                        </div>
                                    </form>
                                    --}}
                                    <!-- User -->

                                    <!-- User -->
                                    <ul class="navbar-nav align-items-center">
                                        <li class="nav-item dropdown">
                                            <a aria-expanded="false" aria-haspopup="true" class="nav-link pr-md-0 mr-md-2 pl-1" data-toggle="dropdown" href="#" role="button">
                                                <div class="media align-items-center">
                                                    <span class="avatar avatar-sm rounded-circle"><img alt="Image placeholder" src="assets1/img/11.png" /></span>
                                                </div>
                                            </a>
                                            <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
                                                <div class="dropdown-header noti-title text-center border-bottom pb-3">
                                                    <h3 class="text-capitalize text-dark mb-1">Administrator</h3>
                                                    {{--
                                                    <h6 class="text-overflow m-0">Administrator</h6>
                                                    --}}
                                                </div>

                                                <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
																		document.getElementById('logout-form').submit();"><i class="ni ni-support-16"></i> <span>Logout</span></a>
                                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
                                            </div>
                                        </li>
                                        {{--
                                        <li class="nav-item dropdown d-none d-md-flex">
                                            <a aria-expanded="false" aria-haspopup="true" title="languages" class="nav-link pr-0" data-toggle="dropdown" href="#" role="button">
                                                <div class="media align-items-center">
                                                    <i class="fe fe-flag f-30"></i>
                                                </div>
                                            </a>
                                            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-arrow dropdown-menu-right">
                                                <a href="#" class="dropdown-item d-flex align-items-center">
                                                    <img src="assets/img/flag-img/french_flag.jpg" alt="flag-img" class="avatar avatar-sm mr-3 align-self-center" />
                                                    <div>
                                                        <strong>French</strong>
                                                    </div>
                                                </a>
                                                <a href="#" class="dropdown-item d-flex align-items-center">
                                                    <img src="assets/img/flag-img/germany_flag.jpg" alt="flag-img" class="avatar avatar-sm mr-3 align-self-center" />
                                                    <div>
                                                        <strong>Germany</strong>
                                                    </div>
                                                </a>
                                                <a href="#" class="dropdown-item d-flex align-items-center">
                                                    <img src="assets/img/flag-img/italy_flag.jpg" alt="flag-img" class="avatar avatar-sm mr-3 align-self-center" />
                                                    <div>
                                                        <strong>Italy</strong>
                                                    </div>
                                                </a>
                                                <a href="#" class="dropdown-item d-flex align-items-center">
                                                    <img src="assets/img/flag-img/russia_flag.jpg" alt="flag-img" class="avatar avatar-sm mr-3 align-self-center" />
                                                    <div>
                                                        <strong>Russia</strong>
                                                    </div>
                                                </a>
                                                <a href="#" class="dropdown-item d-flex align-items-center">
                                                    <img src="assets/img/flag-img/spain_flag.jpg" alt="flag-img" class="avatar avatar-sm mr-3 align-self-center" />
                                                    <div>
                                                        <strong>Spain</strong>
                                                    </div>
                                                </a>
                                            </div>
                                        </li>
                                        <li class="nav-item dropdown d-none d-md-flex">
                                            <a aria-expanded="false" aria-haspopup="true" class="nav-link pr-0" data-toggle="dropdown" href="#" role="button">
                                                <div class="media align-items-center">
                                                    <i class="fe fe-bell f-30"></i>
                                                </div>
                                            </a>
                                            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-arrow dropdown-menu-right">
                                                <a href="#" class="dropdown-item d-flex">
                                                    <div>
                                                        <strong>Someone likes our posts.</strong>
                                                        <div class="small text-muted">3 hours ago</div>
                                                    </div>
                                                </a>
                                                <a href="#" class="dropdown-item d-flex">
                                                    <div>
                                                        <strong> 3 New Comments</strong>
                                                        <div class="small text-muted">5 hour ago</div>
                                                    </div>
                                                </a>
                                                <a href="#" class="dropdown-item d-flex">
                                                    <div>
                                                        <strong> Server Rebooted.</strong>
                                                        <div class="small text-muted">45 mintues ago</div>
                                                    </div>
                                                </a>
                                                <div class="dropdown-divider"></div>
                                                <a href="#" class="dropdown-item text-center">View all Notification</a>
                                            </div>
                                        </li>
                                        --}}
                                        <li class="nav-item d-none d-md-flex">
                                            <div class="dropdown d-none d-md-flex mt-2">
                                                <a class="nav-link full-screen-link pr-0"><i class="fe fe-maximize-2 floating" id="fullscreen-button"></i></a>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </nav>
                            <!-- Top navbar-->
                            <!-- Page content -->
                            <div class="container-fluid pt-8">
                                @yield('content')

                                <!-- Footer -->
                                <footer class="footer">
                                    <div class="row align-items-center justify-content-xl-between">
                                        <div class="col-xl-6">
                                            <div class="copyright text-center text-xl-left text-muted">
                                                <p class="text-sm font-weight-500">Copyright 2018 © All Rights Reserved. Adon Dashboard Template</p>
                                            </div>
                                        </div>
                                        <div class="col-xl-6">
                                            <p class="float-right text-sm font-weight-500">Designed &amp; Passion With: <a href="">Spurko</a></p>
                                        </div>
                                    </div>
                                </footer>
                                <!-- Footer -->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- app-content-->
            </div>
        </div>
       
        <!-- Back to top -->
        <a href="#top" id="back-to-top"><i class="fa fa-angle-up"></i></a>

        <!-- Adon Scripts -->

        <!-- Core -->
        <script src="assets1/plugins/jquery/dist/jquery.min.js"></script>
        <script src="assets1/js/popper.js"></script>
        <script src="assets1/plugins/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets1/plugins/chart-circle/circle-progress.min.js"></script>

        <!-- Optional JS -->
        <script src="assets1/plugins/chart.js/dist/Chart.min.js"></script>
        <script src="assets1/plugins/chart.js/dist/Chart.extension.js"></script>

        <!-- Fullside-menu Js-->
        <script src="assets1/plugins/toggle-sidebar/js/sidemenu.js"></script>

        <!-- Custom scroll bar Js-->
        <script src="assets1/plugins/customscroll/jquery.mCustomScrollbar.concat.min.js"></script>

        <!-- peitychart -->
        <script src="assets1/plugins/peitychart/jquery.peity.min.js"></script>
        <script src="assets1/plugins/peitychart/peitychart.init.js"></script>

        <!-- Data tables -->
        <script src="assets1/plugins/datatable/jquery.dataTables.min.js"></script>
        <script src="assets1/plugins/datatable/dataTables.bootstrap4.min.js"></script>
        <script src="assets1/plugins/datatable/dataTables.responsive.min.js"></script>
        <script src="assets1/plugins/datatable/responsive.bootstrap4.min.js"></script>

        <!-- Adon JS -->
        <script src="assets1/js/custom.js"></script>
        <script src="assets1/js/dashboard-finance.js"></script>
        <script src="assets1/js/datatable.js"></script>
        
    </body>
 
</html>
