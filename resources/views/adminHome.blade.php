@extends('layout.menu') @section('title') Admin || Dashboard @endsection @section('content')
<div class="page-header mt-0 p-3">
    <a class="mb-sm-0" href="/sublogin">Login</a>
    <ol class="breadcrumb mb-0">
        <li class="breadcrumb-item">
            <a href="#"><i class="fe fe-home"></i></a>
        </li>
        <li class="breadcrumb-item active" aria-current="page">Admin Dashboard</li>
    </ol>
</div>
<div class="row finance-content">
    <div class="col-xl-3 col-md-6">
        <div class="card shadow text-center">
            <div class="card-body">
                <h3 class="mb-3">Gross profit Margin</h3>
                <div class="chart-circle" data-value="0.75" data-thickness="10" data-color="#ad59ff">
                    <canvas width="128" height="128"></canvas>
                    <div class="chart-circle-value"><div class="text-xxl">75%</div></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-3 col-md-6">
        <div class="card shadow text-center">
            <div class="card-body">
                <h3 class="mb-3">Opex Ratio</h3>
                <div class="chart-circle" data-value="0.55" data-thickness="10" data-color="#00d9bf">
                    <canvas width="128" height="128"></canvas>
                    <div class="chart-circle-value"><div class="text-xxl">55%</div></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-3 col-md-6">
        <div class="card shadow text-center">
            <div class="card-body">
                <h3 class="mb-3">Operating Profit Margin</h3>
                <div class="chart-circle" data-value="0.30" data-thickness="10" data-color="#fc0">
                    <canvas width="128" height="128"></canvas>
                    <div class="chart-circle-value"><div class="text-xxl">30%</div></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-3 col-md-6">
        <div class="card shadow text-center">
            <div class="card-body">
                <h3 class="mb-3">Net Profit Margin</h3>
                <div class="chart-circle" data-value="0.45" data-thickness="10" data-color="#00b3ff">
                    <canvas width="128" height="128"></canvas>
                    <div class="chart-circle-value"><div class="text-xxl">45%</div></div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
