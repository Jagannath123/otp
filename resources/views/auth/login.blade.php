<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="Fully Responsive Bootstrap 4 Admin Dashboard Template">
	<meta name="author" content="Creative Tim">

	<!-- Title -->
	<title> Admin</title>
    
	<!-- Favicon -->
	<link href="assets1/img/brand/favicon.png" rel="icon" type="image/png">

	<!-- Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Nunito:300,400,600,700,800" rel="stylesheet">

	<!-- Icons -->
	<link href="assets1/css/icons.css" rel="stylesheet">

	<!--Bootstrap.min css-->
	<link rel="stylesheet" href="assets1/plugins/bootstrap/css/bootstrap.min.css">

	<!-- Adon CSS -->
	<link href="assets1/css/dashboard.css" rel="stylesheet" type="text/css">

	<!-- Single-page CSS -->
	<link href="assets1/plugins/single-page/css/main.css" rel="stylesheet" type="text/css">

</head>

<body class="bg-gradient-primary">
	<div class="page">
		<div class="page-main">
			<div class="limiter">
				<div class="container-login100">
						
					<div class="wrap-login100 p-5">
                    	<form class="login100-form validate-form"  method="POST" action="{{ route('login') }}">
                        	@csrf
							<div class="logo-img text-center pb-3">
                            <!-- <h2>LOGO</h2> -->
								<img src="assets/img/logo/logo.png" alt="logo-img" style="width: 60%;">
							
                            </div>
							<span class="login100-form-title">
								Admin Login
							</span>
                            @if(Session::has('message'))
                                <div class="alert alert-danger alert-block">
                                    <button type="button" class="close" data-dismiss="alert">×</button>	
                                        <strong>{{ Session::get('message') }}</strong>
                                </div>
                                @endif
                                <div class="wrap-input100 validate-input" data-validate = "Valid email is required: ex@abc.xyz">
                            <input id="email" type="email" class="input100 @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" placeholder="Email" required autocomplete="email"  autofocus>
                                        
                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                <span class="focus-input100"></span>
								<span class="symbol-input100">
									<i class="fa fa-envelope" aria-hidden="true"></i>
								</span>
							</div>

                            <input type="hidden" name="role" value="1">

							<div class="wrap-input100 validate-input" data-validate = "Password is required">
                            <input id="password" type="password" class="input100 @error('password') is-invalid @enderror" name="password" placeholder="Password" required autocomplete="current-password">

                       
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror	
                                	<span class="focus-input100"></span>
								<span class="symbol-input100">
									<i class="fa fa-lock" aria-hidden="true"></i>
								</span>
							</div>

							<div class="container-login100-form-btn">
								<!-- <a href="index.html" class="login100-form-btn btn-primary">
									Login
								</a> -->
                                <button class="login100-form-btn btn-primary" type="submit"> Log In </button>
							</div>

						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Adon Scripts -->
	<!-- Core -->
	<script src="assets1/plugins/jquery/dist/jquery.min.js"></script>
	<script src="assets1/js/popper.js"></script>
	<script src="assets1/plugins/bootstrap/js/bootstrap.min.js"></script>

</body>
</html>
